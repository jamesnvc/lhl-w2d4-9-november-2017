//
//  SecondViewController.m
//  NotificationsDemo
//
//  Created by James Cash on 09-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

// For KVO method
@property (strong,nonatomic) NSString *typedText;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // equivalent to dragging button to the action using storyboard
    [self.doneButton addTarget:self
                        action:@selector(done:)
              forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender {
    /* // NSNotificationCenter method
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"SecondViewControllerTextChanged"
     object:self
     userInfo:@{@"newText": self.textField.text}];
     */

    // For KVO method
    self.typedText = self.textField.text;

    // Common
    [self dismissViewControllerAnimated:YES completion:^{}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
