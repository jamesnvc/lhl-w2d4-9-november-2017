//
//  ViewController.m
//  NotificationsDemo
//
//  Created by James Cash on 09-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // NSNotificationCenter method
    /*
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateTextDisplay:)
     name:@"SecondViewControllerTextChanged"
     object:nil];
     */

    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] init];
    [self.view addGestureRecognizer:pinch];
    self.view.userInteractionEnabled = YES;
    [pinch addTarget:self action:@selector(mainViewPinch:)];
}

// NSNOtificationCenter method
- (void)updateTextDisplay:(NSNotification*)notification
{
    NSLog(@"Recieved notification %@", notification);
    self.textLabel.text = notification.userInfo[@"newText"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// For KVO method
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;

    [dest addObserver:self
           forKeyPath:@"typedText"
              options:NSKeyValueObservingOptionNew
              context:nil];
}

// For KVO method
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    NSLog(@"Observed KVO change at %@: %@", keyPath, change);
    [object removeObserver:self forKeyPath:@"typedText"];
    self.textLabel.text = change[NSKeyValueChangeNewKey];
}

// Gesture recognizers
- (IBAction)textLabelLongPress:(UILongPressGestureRecognizer *)sender {
    UIView *pressedView = sender.view;
    pressedView.backgroundColor = [UIColor orangeColor];
}

- (void)mainViewPinch:(UIPinchGestureRecognizer*)sender {
    UIView *view = sender.view;
    CGPoint mainLocation = [sender locationInView:self.view];
    CGPoint labelLocation = [sender locationInView:self.textLabel];
    self.textLabel.contentScaleFactor = sender.scale;
    sender.state;

    // LEft as an exercise to the reader

}

@end
